<?php
/*
 @ author Vitaliy Lopatko <agmjm66689@gmail.com> 
*/
// Дано два числа. узнать какое больше!
$a = 42;
$b = 55;

$res = $a > $b ? $a : $b;
echo $res . "\n <br> ";

// Случайное число
$a = 55;
$a = rand(5, 15);
echo $a . "\n <br> ";
$b = 42;
$b = rand(5, 15);
echo $b . "\n <br> ";

// Сокращние
$surname = 'Lopatko';
$name = 'Vitaliy';
$sur_name = 'Sergeevich'; 
$FIO = "$surname  {$name[0]} . {$sur_name[0]}.";
echo $FIO . "\n <br> ";

//Количество вхождений
$num = rand(1, 99999);
$num1 =rand(0, 9);
$res = substr_count($num, $num1);
echo $res . "\n <br> ";

/*Переменные:
   les1 */
$a = 3;
echo $a . "\n <br> ";

//   les2
$a = 10;
$b = 2;
echo $a + $b . "\n <br> ";
echo $a - $b . "\n <br> ";
echo $a / $b . "\n <br> ";
echo $a * $b . "\n <br> ";
 
//les3
 $c = 15;
 $d = 2;
 $result = $c + $d;
 echo $result . "\n <br> ";
 
 //les4
 $a = 10;
 $b = 2;
 $c = 5;
 $result = $a + $b + $c;
 echo $result . "\n <br> ";
 
 //les5
 $a = 17;
 $b = 10;
 $c = $a - $b;
 $d = $c;
 echo $d . "\n <br> ";
$result = $c + $d;
echo $result . "\n <br> ";

// Строки
// les1
$text = 'Привет, Мир!';
echo $text . "\n <br> ";

//les2
$text1 = 'Привет';
$text2 = 'Мир!';
echo "$text1, $text2" . "\n <br> ";

//les3
$sec = 60;
echo $sec . "\n <br> ";//sec one minute
echo $sec * 60 . "\n <br> "; //sec one hour
echo $sec * 60 * 24 . "\n <br> ";//sec one day
echo $sec * 60 * 24 * 7 . "\n <br> ";// sec one week
echo $sec * 60 * 24 * 7 * 30 . "\n <br> ";// sec one month

//les 4
$var = 1;
$var += 12;
$var -= 14;
$var *=  5;
$var /=  7;
$var %= 1;
echo $var . "\n <br> ";

//les 5
$hour = 23;
$min = 57;
$sec = 32;
echo $hour . ':' .  $min . ':' . $sec . "\n <br> ";

//les 6
$text = 'Я';
$text .=' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text . "\n <br> ";

//les7
$bar = 10;
$foo = 'bar';
echo $$foo . "\n <br> ";

//les 8
$a = 2;
$b = 4;
echo $a++ + $b;
echo $a + ++$b;
echo ++$a + $b++ . "\n <br> ";

//les 9
$a = 'asd';
echo isset($a)  ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo gettype($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_null($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo empty($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_integer($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_double($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_string($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_numeric($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_bool($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_scalar($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_array($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";
$a = 'asd';
echo is_object($a) ? 'переменная существует' : 'переменная не существует' . "\n <br> ";

// numeric
$a = rand(1, 10);
$b = rand(1, 10);
echo $a + $b . "\n <br> ";
echo $a * $b  . "\n <br> ";

$a = rand(2, 10);
$b = rand(2, 10);
echo ($a * $a) + ($b * $b) . "\n <br> ";

$x = rand(1, 10);
$y = rand(1, 10);
$z = rand(1, 10);
echo ($x ++) - 2 * ($z - 2 * $x + $y) . "\n <br> ";

$num = rand(5, 100);
$num = $num / 3;
echo $num * 0.3 . "\n <br> ";
$num = $num / 5;
echo $num * 1.2 . "\n <br> ";

$num = rand(100, 999);
$num1 = rand(100, 999);
$result = $num / 0.4 + $num1 / 0.84;
echo $result . "\n <br> ";


$num = rand(100, 999);
$num = (string)$num;
$num[1] = '0';
echo $num[2] . $num[1] . $num[0] . "\n <br> ";;

$num = rand(10, 50);
echo $num;
echo $num % 2 == 0 ? 'Число четное' : 'Число не четное' . "\n <br> ";


?>
