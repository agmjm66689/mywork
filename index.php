<?php
//my name
$name = 'Vitaliy';
//my age
$age = 31;
//number 'pi'
$pi = pi();
//4 array
$arr = ['alex', 'vova', 'tolya'];
$arr1 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr2 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr3 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HW2</title>
</head>
<body>
<ul>
    // Выводим первые переменные
    <li><?= $name ?></li>
    <li><?= $age ?></li>
<li><?=  $pi ?></li>
</ul>
<ul>
<li>//Выводим первый массив
    <pre><?php print_r($arr)?></pre><br>
</li>
</ul>
<ul>
<li>//Выводим второй массив
    <pre><?php print_r($arr1)?></pre><br>
</li>
</ul>
<ul>
<li>//выводим третий массив
    <pre><?php print_r($arr2)?></pre><br>
</li>
</ul>
<ul>
<li>//Выводим четвертый массив
    <pre><?php print_r($arr3)?></pre><br>
</li>
</ul>


</body>
</html>